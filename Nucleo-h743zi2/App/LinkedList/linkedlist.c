/**
  ******************************************************************************
  * @file    
  * @author  
  * @version V0.0.0
  * @date    20-12-2019
  * @brief    
  ******************************************************************************
  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
 
*/
#include <stdlib.h>
#include "linkedlist.h"

struct node 
{
  int           key;
  void          *item_p;
  void          *next_p;
};

typedef struct node node_t;

struct list 
{
    node_t * head; 
};

// Create new node.
node_t * createnode(int key, const void* item_p);
node_t * createnode(int key, const void* item_p)
{
  node_t * new_node = malloc(sizeof(node_t));
  if (new_node == NULL) {
    return NULL;
  }
  new_node->key = key;
  new_node->item_p = (void*)item_p;
  new_node->next_p = NULL;
  return new_node;
}

//// List

// Creating new list.
list_t * ListMake(void)
{
  list_t * list = malloc(sizeof(list_t));
  if (!list) {
    return NULL;
  }
  list->head = NULL;
  return list;
}

// Reversing the list.
void ListReverse(list_t * list)
{
  node_t * reversed = NULL;
  node_t * current = list->head;
  node_t * temp = NULL;
  while(current != NULL){
    temp = current;
    current = current->next_p;
    temp->next_p = reversed;
    reversed = temp;
  }
  list->head = reversed;
}

// Destring the list.
void ListDestroy(list_t * list)
{
  node_t * current = list->head;
  node_t * next = current;
  while(current != NULL){
    next = current->next_p;
    free(current);
    current = next;
  }
  free(list);
}

////// Items on list

// Finding item on list by key.
void * ListItemFind(list_t * list, int key) 
{
  node_t * current = list->head;
  if(list->head == NULL){
    return NULL;
  }

  for(; current != NULL; current = current->next_p) 
  {
    if(current->key == key)
    {
      return current->item_p;
    }
    //printf("%d\n", current->key);
  }
  return NULL;
}

// Adding item to list by key.
void ListItemAdd(list_t * list, void *item_p, int key)
{
  node_t * current = NULL;
  // Checking head of list, create if need
  if(list->head == NULL){
    list->head = createnode(key, item_p);
  }
  else {
    current = list->head; 
    while (current->next_p != NULL){
      current = current->next_p;
    }
    current->next_p = createnode(key, item_p);
  }
}

// Deleting item on list by key.
void ListItemDelete(list_t * list, int key)
{
  node_t * current = list->head;
  node_t * previous = current;
  while(current != NULL){
    if(current->key == key){
      previous->next_p = current->next_p;
      if(current == list->head)
        list->head = current->next_p;
      free(current);
      return;
    }
    previous = current;
    current = current->next_p;
  }
}

// Returns the number of items.
int ListItemCount(const list_t *list)
{
  int cnt = 0;
  if(list == NULL || list->head == NULL) return cnt;
  cnt++;
  node_t * current = list->head;
  while (current->next_p != NULL){
    cnt++;
    current = current->next_p;
  }
return cnt;
}
