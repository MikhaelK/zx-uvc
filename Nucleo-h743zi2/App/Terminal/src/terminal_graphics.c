// Header: Terminal grafics
// File Name: terminal_grafics.c
// Author: Kargapolcev M. E.
// Date: 08.03.2022

#include "terminal_graphics.h"
#include "term.h"

#define BACK_GROUND       Dark_gray
#define INK               Light_green

void TG_init(void) {
  tg_list = ListMake();
  if(tg_list == NULL) {printf("TG list create fail!\r\n"); return;}
  home();
  clear();
  //terminal_frame();
  Bell();
  //printf("\r\n");
}

void TG_add_item_int(char* name, int* var, uint8_t x, uint8_t y) {
  TG_add_item(name, (void*)var, tg_type_int, x, y);
}

void TG_add_item_float(char* name, float* var, uint8_t x, uint8_t y) {
  TG_add_item(name, (void*)var, tg_type_float, x, y);
}

void TG_add_item(char* name, void* var, tg_item_type_t type, uint8_t x, uint8_t y) {
  if(tg_list == NULL) {printf("TG list does not init!\r\n"); return;}
  tg_object_t * item = malloc(sizeof(tg_object_t));
  if(item == NULL) {printf("Cant malloc memory for item %s !\r\n", name); return;}
  item->name  = name;
  item->var   = var;
  item->type  = type;
  char temp[20];
  int len = sgotoxy(temp, x, y);
  item->coordinate = malloc(len);
  strcpy(item->coordinate, temp);
  int _count = ListItemCount(tg_list);
  ListItemAdd(tg_list, (void*)item, _count);
  tg_list_count = _count+1;
}


//int clear_cnt = 0;
void TG_update(void) {
  //if(!(clear_cnt++%1000)) {clear();}

  for(int n = 0; n < tg_list_count; n++) {
    tg_object_t *item = ListItemFind(tg_list, n);
    switch (item->type) {
      case tg_type_int:
        printf("%s%s\t%d", item->coordinate, item->name, *(int*)(item->var));
        break;

      case tg_type_float:
        printf("%s%s\t%.2f", item->coordinate, item->name, *(float*)(item->var));
        break;
      
      default:
        break;
    }
  }
  printf("\n");
}

// Frame for GUI
void terminal_frame(void) {
  set_atrib(BACK_GROUND);
  set_atrib(INK);
  home();
  clear();
  //              1         2         3         4         5         6         7         8
  //     12345678901234567890123456789012345678901234567890123456789012345678901234567890
  puts( "┌──────────────────────────────────────────────────────────────────────────────┐\n\r" //0
        "│                                                                              │\n\r" //1
        "├──────────────────────────────────────────────────────────────────────────────┤\n\r" //2
        "│                                                                              │\n\r" //3
        "│                                                                              │\n\r" //4
        "│                                                                              │\n\r" //5
        "│                                                                              │\n\r" //6
        "│                                                                              │\n\r" //7
        "│                                                                              │\n\r" //8
        "└──────────────────────────────────────────────────────────────────────────────┘\n\r"); 
}

  /* Symbol libriary */
  /* └ ┘ ┌ ┐ ─ │ ├ ┤ */
  /*
2500  ─ ━ 	│ 	┃ 	┄ 	┅ 	┆ 	┇ 	┈ 	┉ 	┊ 	┋ 	┌ 	┍ 	┎ 	┏
2510   ┐ 	┑ 	┒ 	┓ 	└ 	┕ 	┖ 	┗ 	┘ 	┙ 	┚ 	┛ 	├ 	┝ 	┞ 	┟
2520   ┠ 	┡ 	┢ 	┣ 	┤ 	┥ 	┦ 	┧ 	┨ 	┩ 	┪ 	┫ 	┬ 	┭ 	┮ 	┯
2530   ┰ 	┱ 	┲ 	┳ 	┴ 	┵ 	┶ 	┷ 	┸ 	┹ 	┺ 	┻ 	┼ 	┽ 	┾ 	┿
2540   ╀ 	╁ 	╂ 	╃ 	╄ 	╅ 	╆ 	╇ 	╈ 	╉ 	╊ 	╋ 	╌ 	╍ 	╎ 	╏
2550   ═ 	║ 	╒ 	╓ 	╔ 	╕ 	╖ 	╗ 	╘ 	╙ 	╚ 	╛ 	╜ 	╝ 	╞ 	╟
2560   ╠ 	╡ 	╢ 	╣ 	╤ 	╥ 	╦ 	╧ 	╨ 	╩ 	╪ 	╫ 	╬ 	╭ 	╮ 	╯
2570   ╰ 	╱ 	╲ 	╳ 	╴ 	╵ 	╶ 	╷ 	╸ 	╹ 	╺ 	╻ 	╼ 	╽ 	╾ 	╿
2580   ▀ 	▁ 	▂ 	▃ 	▄ 	▅ 	▆ 	▇ 	█ 	▉ 	▊ 	▋ 	▌ 	▍ 	▎ 	▏
2590   ▐ 	░ 	▒ 	▓ 	▔ 	▕ 	▖ 	▗ 	▘ 	▙ 	▚ 	▛ 	▜ 	▝ 	▞ 	▟*/