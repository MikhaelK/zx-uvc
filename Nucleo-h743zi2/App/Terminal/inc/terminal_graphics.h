// Header: Terminal grafics
// File Name: terminal_graphics.h
// Author: Kargapolcev M. E.
// Date: 08.03.2022

#ifndef TG
#define TG
#include "main.h"

void TG_init(void);
void TG_update(void);
void TG_add_item_int(char* name, int* var, uint8_t x, uint8_t y);
void TG_add_item_float(char* name, float* var, uint8_t x, uint8_t y);

#endif /* TG*/
