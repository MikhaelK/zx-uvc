// Header: Terminal grafics
// File Name: terminal_grafics.h
// Author: Kargapolcev M. E.
// Date: 08.03.2022

#ifndef TERM
#define TERM

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "linkedlist.h"

#define ESC "\033"

//Format text
#define RESET       0
#define BRIGHT      1
#define DIM         2
#define UNDERSCORE  3
#define BLINK       4
#define REVERSE     5
#define HIDDEN      6

//Foreground Colours (text)

#define F_BLACK     30
#define F_RED       31
#define F_GREEN     32
#define F_YELLOW    33
#define F_BLUE      34
#define F_MAGENTA   35
#define F_CYAN      36
#define F_WHITE     37
#define Light_green 92

//Background Colours
#define B_BLACK     40
#define B_RED       41
#define B_GREEN     42
#define B_YELLOW    44
#define B_BLUE      44
#define B_MAGENTA   45
#define B_CYAN      46
#define B_WHITE     47
#define Dark_gray   100



#define sgotoxy(buf, x,y) (sprintf(buf, ESC "[%d;%dH", y, x))
#define home()            (printf(ESC "[H")) //Move cursor to the indicated row, column (origin at 1,1)
#define clear()           (printf(ESC "[2J")) //lear the screen, move to (1,1)
#define gotoxy(x,y)       (printf(ESC "[%d;%dH", y, x))
#define visible_cursor()  (printf(ESC "[?251"))
#define resetcolor()      (printf(ESC "[0m"))
#define set_atrib(color)  (printf(ESC "[%dm", color))
//#define up()              (printf(ESC "[1A"))
//#define down()            (printf(ESC "[1B"))
#define Query_Cur_Pos()   (printf(ESC "[6n"))
#define Query_Dev_Stat()  (printf(ESC "[5n"))
#define Title()           (printf(ESC "[2<STM32 Terminal>\x07"))
#define Bell();           (printf("\x07"))

// Рамка. Для красоты.
void terminal_frame(void);

typedef enum tg_item_type {
    tg_type_int   = 0,
    tg_type_float = 1,
}tg_item_type_t;

void TG_add_item(char* name, void* var, tg_item_type_t type, uint8_t x, uint8_t y);

// Список обьектов для отображения.
list_t * tg_list;
int tg_list_count = 0;

typedef struct tg_object {
  char* name;
  void* var;
  char* coordinate;
  tg_item_type_t type;
}tg_object_t;

#endif /* TERM */
