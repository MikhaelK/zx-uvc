module test7064s
(
	// Стандартные сигналы для скандаблера.
	input zx_red,
	input zx_green,
	input zx_blue,
	input zx_bright,
	input zx_hs, // строчная
	input zx_vs, // кадровая
	input zx_clk14,
	
	// Клок от внешней PLL.
	input clk_pll,
	
	// SPI с видеопотоком. CPLD мастер.
	output spi_v_clk,
	output spi_v_mosi,
	output spi_v_nss,
	
	// Прерывание в STM`ке
	output stm_ext,
	
	// SPI с видеопотоком 2. CPLD мастер.
	output  spi_t_clk,
	output  spi_t_mosi,
	output  spi_t_nss,
	
	// Отладочные ножки.
	output dbg_pin0,
	output dbg_pin1,
	
	// Управление чипом PLL NB3N502
	output pll_s1,
	output pll_s2
	
);
	// Отправляем только нужные пиксели.
	reg  [10:0] pix_cnt;
	always @(posedge clk7) begin
		if (zx_hs) pix_cnt <= pix_cnt + 2'b1;
		else pix_cnt <= 8'b0;
	end
	
	reg  [10:0] line_cnt;
	always @(negedge zx_hs) begin
		if (zx_vs) line_cnt <= line_cnt + 2'b1;
		else line_cnt <= 8'b0;
	end
	
	wire pix_en = (pix_cnt > 63) && (pix_cnt < (64+256));
	wire line_en = (line_cnt > 63) && (line_cnt < (64+192));
	
	wire video_en = pix_en & line_en;
	
	
	// RGB
	reg  data_r;
	reg  data_g;
	reg  data_b;
	reg  data_i;
	always @(negedge zx_clk14) begin
		if(~clk7) data_r <= zx_red;
		if(~clk7) data_g <= zx_green;
		if(~clk7) data_b <= zx_blue;
		if(~clk7) data_i <= zx_bright;
		
	end
	
	// RGB. Отправка пикселей.
	reg  out;
	reg [1:0] count;
	always @(posedge clk_pll) begin
			count <= count + 2'd1;
			if (count == 2'd0) out <= data_r;//zx_red;
			if (count == 2'd1) out <= data_g;//x_green;
			if (count == 2'd2) out <= data_b;//zx_blue;
			if (count == 2'd3) out <= data_i;//zx_bright;
	end

	
	assign spi_v_mosi = video_en & out; 
	assign spi_v_clk  = video_en & clk_pll;
	assign spi_v_nss  = ~line_en;
	
	assign spi_t_mosi = video_en & out; 
	assign spi_t_clk  = video_en & clk_pll;
	assign spi_t_nss  = ~video_en;
	
	assign stm_ext = zx_vs;
	

	// PLL x2 14MHz -> 28MHz
	assign pll_s1 = 1'b0;
	assign pll_s2 = 1'b0;
	
	// Деление 14 МГц на 2 для тактирования пиксель клок.
	wire clk7;
	always @(negedge zx_clk14) begin
		clk_div_cnt <= ~clk_div_cnt;
	end
	reg clk_div_cnt 	= 1'd0;
	assign clk7 = clk_div_cnt;
	
	assign dbg_pin1 = video_en; 
	assign dbg_pin0 = zx_hs;
	
endmodule 
