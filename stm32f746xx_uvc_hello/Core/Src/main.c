/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "rng.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include "gfx.h"
#include "fonts.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void uvc_set(uint32_t x, uint32_t y, uint16_t c) {
  //pic[UVC_VIDEO_HEIGHT - x][/*UVC_VIDEO_WIDTH - */y] = c;
}

// Пентагон 448*320
// Скорпион 448*291
#define ZX_PIX  (256/2)
#define ZX_LINE  (192)
#define ZXPIX (zx_screen[j][k/2])
static uint8_t zx_screen[ZX_LINE][ZX_PIX];
static int frame_flag = 0;
int start_dma = 0;
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {
  if(!start_dma) {
    HAL_SPI_Receive_DMA(&hspi4, zx_screen, sizeof(zx_screen));
    start_dma = 1;
  }
}

void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef * hspi)
{
    if(hspi == &hspi4) {
      HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
      frame_flag = 1;
    }
}

typedef struct pix 
{
  
}pix_t;
uint8_t rgbi2rgb888[6][256];
void init_table_rgbi2rgb888(void);
void init_table_rgbi2rgb888(void) {
  for(int n=0; n<256; n++) {
    rgbi2rgb888[0][n] = ((n & 0b10000000)?(127 + (127*((n & 0b00010000)>>4))):(0));
    rgbi2rgb888[1][n] = ((n & 0b01000000)?(127 + (127*((n & 0b00010000)>>4))):(0));
    rgbi2rgb888[2][n] = ((n & 0b00100000)?(127 + (127*((n & 0b00010000)>>4))):(0));
    rgbi2rgb888[3][n] = ((n & 0b00001000)?(127 + (127*((n & 0b00000001)>>0))):(0));
    rgbi2rgb888[5][n] = ((n & 0b00000100)?(127 + (127*((n & 0b00000001)>>0))):(0));
    rgbi2rgb888[6][n] = ((n & 0b00000010)?(127 + (127*((n & 0b00000001)>>0))):(0));
  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  MX_USB_DEVICE_Init();
  MX_RNG_Init();
  MX_SPI5_Init();
  MX_DMA_Init();
  MX_SPI3_Init();
  MX_SPI4_Init();
  /* USER CODE BEGIN 2 */
  HAL_Delay(1);
    init_table_rgbi2rgb888();
  GFX_GotoXY(10, 10);
  GFX_Puts("Hello World! ZX Spectrum!", &Font_7x10, GFX_COLOR_BLUE);
  while(HAL_GPIO_ReadPin(VSync_GPIO_Port, VSync_Pin)){__asm("nop");}
  //HAL_SPI_Receive_DMA(&hspi4, zx_screen, sizeof(zx_screen));

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
  {
    while(frame_flag == 0) {__asm("NOP");}
  if(!HAL_GPIO_ReadPin(USER_Btn_GPIO_Port, USER_Btn_Pin)) {
      for(int j = 0; j < UVC_VIDEO_HEIGHT-1; j++) {
        for(int k = 0; k < UVC_VIDEO_WIDTH-1; k++) {
            pic[(UVC_VIDEO_HEIGHT-1)-j][k].r = (k%2)?(((ZXPIX & 0b00001000)?(127 + (127*(ZXPIX & 0b00000001))):(0))):((ZXPIX & 0b10000000)?(127 + (127*((ZXPIX & 0b00010000)>>4))):(0));
            pic[(UVC_VIDEO_HEIGHT-1)-j][k].g = (k%2)?(((ZXPIX & 0b00000100)?(127 + (127*(ZXPIX & 0b00000001))):(0))):((ZXPIX & 0b01000000)?(127 + (127*((ZXPIX & 0b00010000)>>4))):(0));
            pic[(UVC_VIDEO_HEIGHT-1)-j][k].b = (k%2)?(((ZXPIX & 0b00000010)?(127 + (127*(ZXPIX & 0b00000001))):(0))):((ZXPIX & 0b00100000)?(127 + (127*((ZXPIX & 0b00010000)>>4))):(0));
        }
      }
    } else {
      for(int j = 0; j < UVC_VIDEO_HEIGHT-1; j++) {
        for(int k = 0; k < UVC_VIDEO_WIDTH-1; k++) {
          pic[(UVC_VIDEO_HEIGHT-1)-j][k].r = (k%2)?(rgbi2rgb888[3][ZXPIX]):((rgbi2rgb888[0][ZXPIX]));
          pic[(UVC_VIDEO_HEIGHT-1)-j][k].g = (k%2)?(rgbi2rgb888[4][ZXPIX]):((rgbi2rgb888[1][ZXPIX]));
          pic[(UVC_VIDEO_HEIGHT-1)-j][k].b = (k%2)?(rgbi2rgb888[5][ZXPIX]):((rgbi2rgb888[2][ZXPIX]));
        }
      }
    }
    frame_flag = 0;
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO2, RCC_MCO2SOURCE_HSE, RCC_MCODIV_1);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

