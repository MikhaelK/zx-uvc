#include "gfx.h"
#include "_gfx.h"

void GFX_Circle(uint16_t x, uint16_t y, uint16_t radius, uint16_t color) {
  int xc = 0;
  int yc = 0;
  int p = 0;
  // Out of range
  if (x >= GFX_WIDTH || y >= GFX_HEIGHT) return;

  yc = radius;
  p = 3 - (radius<<1);
  while (xc <= yc)
  {
    GFX_PUT(x + xc, y + yc, color);
    GFX_PUT(x + xc, y - yc, color);
    GFX_PUT(x - xc, y + yc, color);
    GFX_PUT(x - xc, y - yc, color);
    GFX_PUT(x + yc, y + xc, color);
    GFX_PUT(x + yc, y - xc, color);
    GFX_PUT(x - yc, y + xc, color);
    GFX_PUT(x - yc, y - xc, color);
    if (p < 0) p += (xc++ << 2) + 6;
    else p += ((xc++ - yc--)<<2) + 10;
  }
}