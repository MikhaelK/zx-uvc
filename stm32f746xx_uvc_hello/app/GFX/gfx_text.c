#include "gfx.h"

// Write point for GFX_Putc
void GFX_GotoXY(uint16_t x, uint16_t y) {
  X = x;
  Y = y;
}

// Draw char
char GFX_Putc(char ch, FontDef_t* Font, uint16_t color) {
  uint32_t i, b, j;
  
  /* Check available space in display */
  if (GFX_WIDTH < (X + Font->FontWidth) || GFX_HEIGHT < (Y + Font->FontHeight)) {return 0;}
  
  /* Go through font */
  for (i = 0; i < Font->FontHeight; i++) {
    b = Font->data[(ch - 32) * Font->FontHeight + i];
    for (j = 0; j < Font->FontWidth; j++) {
      if ((b << j) & 0x8000) {
        GFX_PUT(Y + i, (X + j), color);
      } else {
        GFX_PUT(Y + i, (X + j), 0); // TODO: background color
      }
    }
  }
  
  /* Increase pointer */
  X += Font->FontWidth;
  
  /* Return character written */
  return ch;
}

// Draw string
char GFX_Puts(const char* str, void* Font, uint16_t color) {
  /* Write characters */
  while (*str) {
    /* Write character by character */
    if (GFX_Putc(*str, Font, color) != *str) {
      /* Return error */
      return *str;
    }
    
    /* Increase string pointer */
    str++;
  }
  
  /* Everything OK, zero should be returned */
  return *str;
}