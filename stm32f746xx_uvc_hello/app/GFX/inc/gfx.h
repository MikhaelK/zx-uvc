#ifndef _GFX_
#define _GFX_

#include "main.h"
#include "fonts.h"
#include "gfx_color.h"
#include <stdio.h>
//#define GFX_WIDTH  (240U)
//#define GFX_HEIGHT (320U)

// Display driver


// Display Func
#define GFX_PUT(x, y, c)    uvc_set(x, y, c)

// Display Property
#define GFX_WIDTH           UVC_VIDEO_WIDTH
#define GFX_HEIGHT          UVC_VIDEO_HEIGHT

// GFX pixel coordinate
uint16_t X, Y;

// Init
//void GFX_Init(void);

// Update screen
//void GFX_Update(void);

// Draw circle
void GFX_Circle(uint16_t x, uint16_t y, uint16_t radius, uint16_t color);

// Write point for GFX_Puts
void GFX_GotoXY(uint16_t x, uint16_t y);

// Draw string
char GFX_Puts(const char* str, void* Font, uint16_t color);

#endif /* _GFX_ */