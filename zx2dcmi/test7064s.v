module test7064s
(
	// Стандартные сигналы для скандаблера.
	input zx_red,
	input zx_green,
	input zx_blue,
	input zx_bright,
	input zx_hs, // строчная
	input zx_vs, // кадровая
	input zx_clk14,
	
	// DCMI
	output dcmi_hsync,
	output dcmi_vsync,
	output dcmi_pixclk,
	output dcmi_d0,
	output dcmi_d1,
	output dcmi_d2,
	output dcmi_d3,
	output dcmi_d4,
	output dcmi_d5,
	output dcmi_d6,
	output dcmi_d7,
	
	// Отладочные ножки.
	output dbg_pin0,
	output dbg_pin1,
	
	// Клок от внешней PLL.
	input clk_pll,
	
	// Управление чипом PLL NB3N502
	output pll_s1,
	output pll_s2
	
);
	reg reg_dcmi_d0;
	reg reg_dcmi_d1;
	reg reg_dcmi_d2;
	reg reg_dcmi_d3;
	reg reg_dcmi_d4;
	reg reg_dcmi_d5;
	reg reg_dcmi_d6;
	reg reg_dcmi_d7;
	
	always @(negedge clk7) begin
		reg_dcmi_d0 <= zx_red;
		reg_dcmi_d1 <= zx_green;
		reg_dcmi_d2 <= zx_blue;
		reg_dcmi_d3 <= zx_bright;
		reg_dcmi_d4 <= zx_red;
		reg_dcmi_d5 <= zx_green;
		reg_dcmi_d6 <= zx_blue;
		reg_dcmi_d7 <= zx_bright;
	end

	assign dcmi_d0 = reg_dcmi_d0;
	assign dcmi_d1 = reg_dcmi_d1;
	assign dcmi_d2 = reg_dcmi_d2;
	assign dcmi_d3 = reg_dcmi_d3;
	assign dcmi_d4 = reg_dcmi_d4;
	assign dcmi_d5 = reg_dcmi_d5;
	assign dcmi_d6 = reg_dcmi_d6;
	assign dcmi_d7 = reg_dcmi_d7;
	assign dcmi_hsync = zx_hs;
	assign dcmi_vsync = zx_vs;
	assign dcmi_pixclk = clk7;
	
	// PLL x2 14MHz -> 28MHz
	assign pll_s1 = 1'b0;
	assign pll_s2 = 1'b0;
	
	// Деление 14 МГц на 2 для тактирования пиксель клок.
	wire clk7;
	always @(negedge zx_clk14) begin
		clk_div_cnt <= ~clk_div_cnt;
	end
	reg clk_div_cnt 	= 1'd0;
	assign clk7 = clk_div_cnt;
	
	assign dbg_pin1 = zx_vs; 
	assign dbg_pin0 = zx_hs;
	
endmodule 
